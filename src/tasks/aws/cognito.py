import logging
import tasks.util.menu
import tasks.util.aws

from invoke import task
from datetime import datetime, timedelta, timezone
from tasks.util.menu_item import MenuItem

SERVICE_NAME = "cognito-idp"
logger = logging.getLogger('congnito-tasks')


@task(help={'user_pool_id': "UserPoolID to purge"}, optional=['user_pool_id'])
def purge_unconfirmed(c, user_pool_id=None):
    """
    Purge users that have been in the UNCONFIRMED state for more than 2 days in the UserPool
    """
    client = tasks.util.aws.init_aws_client(c['aws-profile'], SERVICE_NAME)

    if user_pool_id is None:
        user_pool_id = get_user_pool_id(client)

    response = client.list_users(
        UserPoolId=user_pool_id,
        Filter="cognito:user_status=\"UNCONFIRMED\""
     )

    users = response.get("Users")
    cutoff_date = datetime.now().replace(tzinfo=timezone.utc) - timedelta(days=2)

    deleted = 0
    for user in users:
        if user.get('UserCreateDate') < cutoff_date:
            delete_single_user(c, user.get("Username"), user_pool_id)
            deleted += 1

    logger.info(f"Deleted {deleted} users")


@task(help={'username': "User to delete", 'user_pool_id': "User pool ID user belongs to"})
def delete_single_user(c, username, user_pool_id):
    """
    Delete a single user from the User Pool
    """

    logger.info(f"Deleting {username} from {user_pool_id}")

    client = tasks.util.aws.init_aws_client(c['aws-profile'], SERVICE_NAME)

    client.admin_delete_user(
        UserPoolId=user_pool_id,
        Username=username
    )


def get_user_pool_id(client):
    """
    Get the user pool id to action on if not provided
    :return:
    """

    response = client.list_user_pools(
        MaxResults=60
    )

    list_user_pools = response.get("UserPools")

    key_id_map = dict()
    key = 1

    for user_pool in list_user_pools:
        user_pool_item = MenuItem(user_pool.get("Id"), user_pool.get("Name"))
        key_id_map[key] = user_pool_item
        key += 1

    print("Please choose the id you want to action on")
    selected_user_pool = tasks.util.menu.get_selection(key_id_map)

    return selected_user_pool
