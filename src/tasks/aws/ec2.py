import os
import logging
import tasks.util.aws
import tasks.util.menu
import json
import time

from invoke import task
from tasks.util.aws import map_key_values_for_menu

SERVICE_NAME = "ec2"
logger = logging.getLogger('ec2')

TERRAFORM_JSON_FILE = "tf.json"
TERRAFORM_JSON = dict.fromkeys(['provider', 'resource', 'output'])

INSTANCE_WHITELIST = {}


@task(help={'key_name': "Unique name for key pair",
            'output_dir': "Directory to save private key",
            'dry_run': "Dry run"})
def create_key_pair(c, key_name, output_dir, dry_run=True):
    """
    Creates a key pair in the region of active profile.
    Saves key pair in specified location
    """

    if not os.path.isdir(output_dir):
        logger.error(f"{output_dir} does not exist.")
        print(f"{output_dir} does not exist. Do you want to create it? [Y/N]")
        choice = input().lower()

        if choice == "y":
            logger.info(f"Creating {output_dir}")
            os.makedirs(output_dir)
        else:
            logger.error(f"Aborting creating key pair. {output_dir} does not exist.")
            exit(1)

    client = tasks.util.aws.init_aws_client(c['aws-profile'], SERVICE_NAME)

    response = client.create_key_pair(
        KeyName=key_name,
        DryRun=dry_run
    )

    local_pri_key_file = os.path.join(output_dir, f"{key_name}.pem")

    logger.info(f"Saving private key as {local_pri_key_file}")
    with open(local_pri_key_file, "w+") as pri_key:
        pri_key.write(response.get('KeyMaterial'))

    logging.info(f"Key saved to {output_dir}")


@task()
def generate_terraform_ec2_config(c, instance_name, output_dir, instance_type="t2.micro", ami="ami-082b5a644766e0e6f"):
    """
    Generates a JSON Terraform config file to deploy a new EC2 instance
    """

    client = tasks.util.aws.init_aws_client(c['aws-profile'], SERVICE_NAME)

    if not os.path.exists(output_dir):
        logging.warning(f"{output_dir} does not exist. Creating...")
        os.makedirs(output_dir)

    # Create Provider
    create_terraform_provider(c['aws-profile'])
    # Create Output
    create_terraform_output(instance_name)
    # Create Resource
    selected_key = select_key_pairs(client)
    selected_security_groups = select_security_group(client)

    create_terraform_resource(instance_name, instance_type, ami, selected_key, selected_security_groups)

    output_filename = f"{instance_name}_{time.time()}.{TERRAFORM_JSON_FILE}"

    with open(os.path.join(output_dir, output_filename), 'w') as output_file:
        json.dump(TERRAFORM_JSON, output_file)

    logging.info(f"Terraform file saved to {output_dir} as {output_filename}")


def select_key_pairs(client):
    """
    Select a key pair to use for creating an EC2 instance
    """

    response = client.describe_key_pairs()

    key_pairs = response.get("KeyPairs")
    key_fingerprint_map = map_key_values_for_menu(key_pairs, "KeyName", "KeyName")

    print("Please choose the KeyPair to use:")
    selected_key = tasks.util.menu.get_selection(key_fingerprint_map)

    return selected_key


def select_security_group(client):
    """
    Select a security group to use for creating an EC2 instance
    :param client:
    :return:
    """

    response = client.describe_security_groups()

    security_groups = response.get("SecurityGroups")
    security_group_map = map_key_values_for_menu(security_groups, "GroupId", "GroupName")

    print("Please choose the SecurityGroup to use:")
    selected_keys = tasks.util.menu.get_list_selection(security_group_map)

    return list(selected_keys)


def create_terraform_provider(profile):
    """
    Creates the Provider config for Terraform
    :return:
    """

    region = tasks.util.aws.get_current_region()
    aws_provider = dict()
    aws_provider['aws'] = {"profile": profile, "region": region}

    TERRAFORM_JSON['provider'] = aws_provider


def create_terraform_resource(instance_name, instance_type, ami, key, security_groups):
    """
    Creates the Resource config for Terraform
    :return:
    """

    instance_config = dict()
    instance_config['instance_type'] = instance_type
    instance_config['ami'] = ami
    instance_config['key_name'] = key
    instance_config['vpc_security_group_ids'] = security_groups

    aws_instance = dict()
    aws_instance['aws_instance'] = {instance_name: instance_config}

    TERRAFORM_JSON['resource'] = aws_instance


def create_terraform_output(instance_name):
    """
    Creates the output section for Terraform
    :return:
    """
    print_public_ip = dict()
    print_public_ip['value'] = f"${{aws_instance.{instance_name}.public_ip}}"

    print_public_dns = dict()
    print_public_dns['value'] = f"${{aws_instance.{instance_name}.public_dns}}"

    output = dict()
    output['instance_public_ip_addr'] = print_public_ip
    output['instance_public_dns_addr'] = print_public_dns

    TERRAFORM_JSON['output'] = output
