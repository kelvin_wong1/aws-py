def get_selection(key_id_map):
    """
    Helper function to print a menu from a map
    :param key_id_map:
    :return: selected value
    """

    while 1:
        try:
            for key in key_id_map:
                menu_item = key_id_map.get(key)
                print(f"{key} >> Name: {menu_item.friendly_name}, ID: {menu_item.aws_key}")

            selection = int(input())

            if selection in key_id_map.keys():
                selected_aws_id = key_id_map.get(selection).aws_key
                break
            else:
                print("Not a valid selection")
        except ValueError:
            print("Please enter a number")

    return selected_aws_id


def get_list_selection(key_id_map):
    """
    Helper function to print a menu from a map. Allows multiple selections
    :param key_id_map:
    :return: selected values
    """

    selections = set()

    while 1:
        selected = get_selection(key_id_map)
        selections.add(selected)

        print("Select an other value? [y/N]")
        confirmation = input()

        if confirmation.lower() == 'n' or None:
            break

    return selections


