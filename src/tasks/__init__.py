import logging
import os

from invoke import Collection
from tasks.aws import cognito, ec2

logging.basicConfig(filename='aws-py.log',
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    level=logging.INFO)

console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
console.setFormatter(formatter)

logging.getLogger('').addHandler(console)
logger = logging.getLogger('tasks-manager')

ns = Collection()

try:
    aws_profile = os.environ['AWS_PROFILE']
    logger.info(f"Using AWS profile {aws_profile}")
    ns.configure({'aws-profile': aws_profile})
except KeyError:
    logger.error('No AWS profile provided')
    exit(1)

ns.add_collection(cognito)
ns.add_collection(ec2)
